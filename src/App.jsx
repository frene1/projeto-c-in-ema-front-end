import "./global.css"
import {BrowserRouter, Routes, Route} from "react-router-dom"
import {PaginaInicial} from "./pages/PaginaInicial/PaginaInicial"
import { Filmes } from "./pages/Filmes/Filmes"
import { PaginaLogin } from "./pages/PaginaLogin/PaginaLogin"
import { PaginaRegistro } from "./pages/PaginaRegistro/PaginaRegistro"
import { Sessoes } from "./pages/Sessoes/Sessoes"
import { PaginaFaleConosco } from "./pages/paginaFaleConosco/PaginaFaleConosco"
import { ApiContextProvider } from "./contexts/ApiContextProvider"
import { PaginaCheckout } from "./pages/Paginacheckout/PaginaCheckout"

function App() {
  return (
    <>
      <BrowserRouter>
        <ApiContextProvider>
          <Routes>
            <Route path="/" element={<PaginaInicial />} />
            <Route path="/filmes" element={<Filmes />} />
            <Route path="/login" element={<PaginaLogin />} />
            <Route path="/registre-se" element={<PaginaRegistro />} />
            <Route path="/sessoes" element={<Sessoes />} />
            <Route path="/fale-conosco" element={<PaginaFaleConosco />} />
            <Route path="/checkout" element={<PaginaCheckout />} />
        </Routes>
        </ApiContextProvider>
      </BrowserRouter>
    </>
  )
}
export default App
