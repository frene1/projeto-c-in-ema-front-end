import styles from './paginacheckout.module.css'

import { Header } from '../../components/Header'
import { Footer } from '../../components/Footer'
import { Aside } from '../../components/Checkout/Aside'
import { Room } from '../../components/Checkout/Room'
import { useSearchParams } from 'react-router-dom'
import { BackgroundIngresso } from "../../components/BackgroundIngresso"

import { useEffect, useState, useContext } from 'react'

import { ApiContext } from '../../contexts/ApiContextProvider';

export function PaginaCheckout () {
    const { basePath } = useContext(ApiContext);
    
    const [ searchParams, setSearchParams ] = useSearchParams();
    const filmeId = searchParams.get("filmeId");
    const sessaoId = searchParams.get("sessaoId");

    const [ filme, setFilme ] = useState();
    const [ sessao, setSessao ] = useState();

    const [ assentosSelecionados, setAssentosSelecionados ] = useState([]);

    function loadFilme() {
        const url = `${basePath}/filmes/id`;
        const config = {
            method: "GET"
        }
        fetch(`${url}/${filmeId}`, config)
        .then(response => { response.json()
            .then(data => {
                setFilme(data);
            })
        })
    }

    function loadSessao() {
        const url = `${basePath}/sessoes/id`;
        const config = {
            method: "GET"
        }
        fetch(`${url}/${sessaoId}`, config)
        .then(response => { response.json()
            .then(data => {
                setSessao(data);
            })
        })
    }

    function handleRoomChange(assento) {
        let updatedAssentosSelecionados = [...assentosSelecionados];

        if (updatedAssentosSelecionados.some(item =>
            JSON.stringify(item) === JSON.stringify(assento)
        )) {
            updatedAssentosSelecionados = updatedAssentosSelecionados.filter(item =>
                JSON.stringify(item) !== JSON.stringify(assento)    
            );
          } else {
            updatedAssentosSelecionados.push(assento);
          }

        setAssentosSelecionados(updatedAssentosSelecionados);
    }

    function handleAsideSubmit() {
        setAssentosSelecionados([]);
    }

    useEffect(() => {
        loadFilme()
        loadSessao()
    }, [filmeId, sessaoId])

    return (
        <div>
            <Header />
            <BackgroundIngresso />
            <div className={styles.backgroudCheckout}>
                { (filme && sessao) &&
                    <Aside
                        filme={filme}
                        sessao={sessao}
                        assentosSelecionados={assentosSelecionados}
                        onSubmit={handleAsideSubmit}
                    />
                }
                <Room
                    sessaoId={sessaoId}
                    onChange={handleRoomChange}
                    assentosSelecionados={assentosSelecionados}
                />
            </div>
            <Footer />
        </div>
    )
}