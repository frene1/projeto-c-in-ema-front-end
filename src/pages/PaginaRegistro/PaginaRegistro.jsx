import { Header } from "../../components/Header"
import { Footer } from "../../components/Footer"
import styles from "./paginaregistro.module.css"

import { useNavigate } from "react-router-dom";


export function PaginaRegistro() {
    const navigate = useNavigate();

    function registerUser(event){
        event.preventDefault()

        const user = {
            "nome": document.getElementById("nome").value,
            "sobrenome": document.getElementById("sobrenome").value,
            "cpf": document.getElementById("cpf").value,
            "dataNascimento": document.getElementById("dataNascimento").value,
            "nomeUsuario": document.getElementById("nomeUsuario").value,
            "email": document.getElementById("email").value,
            "senha": document.getElementById("senha").value
        }
        if (user.senha != document.getElementById("confirmarSenha").value){
            alert("Campo senha e confirmar senha precisam ser iguais")
        }
        else if (!validateEmail(user.email)){
            alert("Email inválido")
        }
        else if (!validateDate(user.dataNascimento)){
            alert("Data inválida")
        }
        else if (!validateCPF(user.cpf)){
            alert("CPF inválido. Deve conter 11 números")
        }
        else if (!validateName(user.nome)){
            alert("Nome inválido")
        }
        else if (!validateName(user.sobrenome)){
            alert("Sobrenome inválido")
        }
        else if (!validatePassword(user.senha)){
            alert("Senha inválida. Deve conter no mínimo 8 caracteres, uma letra maiúscula, uma letra minúscula e um número.")
        }
        else {fetch("http://localhost:4000/usuarios/cadastrar", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
        }).then(response => {
            response.json()
            .then(data => {
                console.log(data)
                if (!data.error) {
                    navigate("/login");
                }
            })
        })} 
    }
    function validateEmail(email){
        const emailRegex = new RegExp(
            /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-z]{2,}$/
        )
        if (emailRegex.test(email)){
            return true
        }
        return false
    }
    function validateCPF(cpf){
        const cpfRegex = new RegExp(
            /^([0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2})$/
        )
        if (cpfRegex.test(cpf)){
            return true
        }
        return false
    }
    function validateName(name){
        const nameRegex = new RegExp(
            /^[a-zA-Z\u00C0-\u00DA ]+$/
        )
        if (nameRegex.test(name)){
            return true
        }
        return false
    }
    function validateDate(date){
        const dateRegex = new RegExp(
            /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/
        )
        if (dateRegex.test(date)){
            return true
        }
        return false
    }
    function validatePassword(password){
        const passwordRegex = new RegExp(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d\S]{8,}$/
        )
        if (passwordRegex.test(password)){
            return true
        }
        return false
    }
    return(
        <div>
            <Header />
            <main>
                <img src="src/assets/Fundo-Ingresso.svg" className={styles.backgroundImage} alt="" />
                <div className={styles.container}>
                    <h1>Junte-se à Comunidade Cinematográfica! Cadastre-se Aqui!</h1>
                    <h2>Seja bem-vindo à nossa comunidade apaixonada pelo mundo do cinema. Ao fazer parte do nosso espaço digital, você está prestes a mergulhar em uma experiência cinematográfica única, onde a magia das telonas ganha vida com um toque moderno.
                    Nosso formulário de cadastro é o primeiro passo para embarcar nessa jornada emocionante. Ao preenchê-lo, você se tornará um membro da nossa comunidade, onde amantes do cinema se reúnem para compartilhar o entusiasmo, as emoções e as histórias que permeiam cada cena.</h2>
                    <form>
                        <h1>Registre-se</h1>
                        <input id="nome" type="text" placeholder="Nome" required/>
                        <input id="sobrenome" type="text" placeholder="Sobrenome" required/>
                        <input id="cpf" type="text" placeholder="CPF" required />
                        <input id="dataNascimento" type="text" placeholder="Data de Nascimento" required />
                        <input id="nomeUsuario" type="text" placeholder="Nome de Usuário" required/>
                        <input id="email" type="text" placeholder="E-mail" required />
                        <input id="senha" type="password" placeholder="Senha" required />
                        <input id ="confirmarSenha" type="password" placeholder="Confirmar Senha" required />
                        <button onClick={registerUser}className={styles.registerButton}>REGISTRAR</button>
                    </form>
                </div>
            </main>
            <Footer />
        </div>
    )
}