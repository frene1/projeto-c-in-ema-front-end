import styles from './filmes.module.css'

import { Header } from '../../components/Header'
import { Footer } from '../../components/Footer'

import { BackgroundIngresso } from '../../components/BackgroundIngresso';

import { useEffect, useState } from 'react';
import { Search } from '../../components/Filmes/Search'
import { FilmeCard } from '../../components/Filmes/FilmeCard';
import { Paginacao } from '../../components/Filmes/Paginacao'


const elementsPerPage = 6;

export function Filmes(){
    const [ listaFilmes, setListaFilmes ] = useState();

    const [ paginaIndex, setPaginaIndex ] = useState(1);
    const [ lastPaginaIndex, setLastPaginaIndex ] = useState(1);

    function searchFilmes({
        titulo=null,
        genero=null,
        classificacao=null
    } = {}) {

        console.log("Pesquisando");
        console.log(`SearchBar: ${titulo}`);
        console.log(`Genero: ${genero} | Classificacao: ${classificacao}`);

        const url = "http://localhost:4000/filmes"
        const config = {
            method: "GET",
        }

        fetch(`${url}?titulo=${titulo}&genero=${genero}&classificacao=${classificacao}`, config)
        .then(response => { response.json()
            .then(data => {
                const updatedListaFilmes = data;
                setListaFilmes(updatedListaFilmes);

                setPaginaIndex(1);

                const updatedLastPaginaIndex = Math.max(
                    Math.floor((updatedListaFilmes.length - 1) / elementsPerPage) + 1,
                    1
                );
                setLastPaginaIndex(updatedLastPaginaIndex);
            })
        })
    }

    useEffect(searchFilmes, []);

    return(
        <>
        <Header/>

        <BackgroundIngresso/>

        <Search searchFilmes={searchFilmes}/>

        <section className={styles.filmes}>
        <section className={styles.filmesWrapper}>
            <h1>Filmes</h1>

            {(listaFilmes && listaFilmes.length == 0) &&
                <h2>Nenhum filme encontrado.</h2>
            }

            <section className={styles.cartazes}>
                {(listaFilmes && listaFilmes.length > 0) &&
                    Array.from({length: elementsPerPage}, (_, i) => (paginaIndex - 1) * elementsPerPage + i).map(
                        index => {
                            if (index >= listaFilmes.length) {
                                return;
                            }

                            const filme = listaFilmes[index];

                            return <FilmeCard key={filme.id} filme={filme}/>
                        }
                    )
                }

            </section>

            <Paginacao
                paginaIndex={paginaIndex}
                lastPaginaIndex={lastPaginaIndex}
                setPaginaIndex={setPaginaIndex}
            />

        </section>
        </section>

        <Footer/>

        </>
    )
}
