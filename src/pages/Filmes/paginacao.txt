Modelo de Paginação de Tamanho Constante Configurável

rangeStart: 2
rangeEnd: 2
rangeCenter: 2

index	array
1       [1, 2] + [3, 4, "...", n-3, n-2] + [n-1, n]         (tipo 1 - "..." centralizada)
2       [1, 2] + [3, 4, "...", n-3, n-2] + [n-1, n]         (tipo 1 - "..." centralizada)
3       [1, 2] + [3, 4, "...", n-3, n-2] + [n-1, n]         (tipo 2 - "..." deslocada)
4       [1, 2] + [3, 4, 5, "...", n-2] + [n-1, n]           (tipo 2 - "..." deslocada)         
5       [1, 2] + [3, 4, 5, 6, "..."] + [n-1, n]             (tipo 2 - "..." deslocada)
6       [1, 2] + ["...", 5, 6, 4, "..."] + [n-1, n]         (tipo 3 - "..." periférica)
k       [1, 2] + ["...", k-1, k, k+1, "..."] + [n-1, n]     (tipo 3 - "..." periférica)
n-5     [1, 2] + ["...", n-6, n-5, n-4, "..."] + [n-1, n]   (tipo 3 - "..." periférica)
n-4     [1, 2] + ["...", n-5, n-4, n-3, n-2] + [n-1, n]    (tipo 3 - "..." deslocada)
n-3     [1, 2] + [3, "...", n-4, n-3, n-2] + [n-1, n]       (tipo 2 - "..." deslocada)
n-2     [1, 2] + [3, 4, "...", n-3, n-2] + [n-1, n]         (tipo 2 - "..." deslocada)
n-1     [1, 2] + [3, 4, "...", n-3, n-2] + [n-1, n]         (tipo 1 - "..." centralizada)
n       [1, 2] + [3, 4, "...", n-3, n-2] + [n-1, n]         (tipo 1 - "..." centralizada)

n = 10
index   array
1       [1, 2] + [3, 4, "...", 7, 8] + [9, 10]      (tipo 1)
2       [1, 2] + [3, 4, "...", 7, 8] + [9, 10]      (tipo 1)
3       [1, 2] + [3, 4, "...", 7, 8] + [9, 10]      (tipo 2)
4       [1, 2] + [3, 4, 5, "...", 8] + [9, 10]      (tipo 2)
5       [1, 2] + [3, 4, 5, 6, "..."] + [9, 10]      (tipo 2)
6       [1, 2] + ["...", 5, 6, 7, 8] + [9, 10]      (tipo 2)
7       [1, 2] + [3, "...", 6, 7, 8] + [9, 10]      (tipo 2)
8       [1, 2] + [3, 4 "...", 7, 8] + [9, 10]       (tipo 2)
9       [1, 2] + [3, 4, "...", 7, 8] + [9, 10]      (tipo 1)
10      [1, 2] + [3, 4, "...", 7, 8] + [9, 10]      (tipo 1)


o primeiro array tem tamanho rangeStart
o array do meio tem tamanho 1 + 2 * rangeCenter
o ultimo array tem tamanho rangeEnd

o primeiro e o último array devem ser gerados primeiro
o array do meio deve ser gerado das pontas para o centro


1. se rangeStart + (1 + 2 * rangeCenter) + rangeEnd >= n
    1.1 retorna um array [1, ..., n]

2. cria arrayInicio [1, ..., rangeStart]
3. startArrayFinal = 1 + n - rangeEnd
4. cria arrayFinal [startArrayFinal, n]

5. se index <= rangeStart OU index >= startArrayFinal (tipo 1 - index fora do arrayCentro)
    4.1 cria arrayCentro = [rangeStart + 1, ..., rangeStart + rangeCenter]
        arrayCentro += ["..."]
        arrayCentro += [startArrayFinal - rangeCenter, startArrayFinal - 1]

(index dentro do arrayCentro)
6. senão se index - (rangeCenter + 1) <= rangeStart (tipo 2 - index borda esquerda)
    6.1 cria o arrayCentro = [rangeStart + 1, ..., index + (rangeCenter - 1)]
    6.2 arrayCentro += ["..."]
    6.3 arrayCentro += [startArrayFinal - ((1 + 2 * rangeCenter) - arrayCentro.length), ..., startArrayFinal - 1]

7. senão se index + (rangeCenter + 1) >= startArrayFinal (tipo 2 - index borda direita)
    7.1 cria arrayCentro = [index - (rangeCenter - 1), ..., startArrayFinal - 1]
    7.2 insere no começo do arrayCentro: ["..."]
    7.3 insere no começo do arrayCentro: [rangeStart + 1, ... rangeStart + (1 + 2 * rangeCenter) - arrayCentro.length]

8. (tipo 3 - index centralizado)
    8.1 cria arrayCentro = ["..."]
    8.2 arrayCentro += [index - rangeCenter, index + rangeCenter]
    8.3 arrayCentro += ["..."]

9. retorna arrayInicio + arrayCentro + arrayFinal
