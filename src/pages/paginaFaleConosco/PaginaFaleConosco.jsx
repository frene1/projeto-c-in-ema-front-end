import { Header } from '../../components/Header'
import { Footer } from '../../components/Footer'
import styles from './paginaFaleConosco.module.css'


export function PaginaFaleConosco(){
    return(
        <>
        <Header />
        <main>
            <img className={styles.backgroundImage} src="src/assets/Fundo-Ingresso.svg" alt="" />
            <div className={styles.contactForm}>
                <h1>Contato</h1>
                <h2>Encontrou algum problema? Envie uma mensagem!</h2>
                <input className={styles.inputStyles} type="text" placeholder='Nome Completo' />
                <input className={styles.inputStyles} type="text" placeholder='Assunto' />
                <textarea className={styles.inputStyles} placeholder="Descrição detalhada"></textarea>
                <button>Enviar</button>
            </div>
        </main>
        <Footer />
        </>
    )
}