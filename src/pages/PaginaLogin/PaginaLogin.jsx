import styles from './paginalogin.module.css'
import { Header } from '../../components/Header'
import { Footer } from '../../components/Footer'
import { NavLink, useNavigate } from 'react-router-dom'

import { useContext } from "react";
import { ApiContext } from '../../contexts/ApiContextProvider';

export function PaginaLogin() {
    const { basePath, setToken, setSecretKey } = useContext(ApiContext);

    const navigate = useNavigate();

    function showPassword(){
        var inputPassword = document.getElementById('password')
        var eyeIcon = document.getElementById('eye-icon')
        var eyeSlashIcon = document.getElementById('eye-slash-icon')
        if(inputPassword.type === 'password'){
            inputPassword.setAttribute('type', 'text')
            eyeIcon.style.display = 'none'
            eyeSlashIcon.style.display = 'block'
        } else {
            inputPassword.setAttribute('type', 'password')
            eyeIcon.style.display = 'block'
            eyeSlashIcon.style.display = 'none'
        }
    }

    function handleLoginFromSubmit(event) {
        event.preventDefault();
        const inputUsuario = document.getElementById("usuario").value;
        const inputSenha = document.getElementById("password").value;

        let fetchBody = {
            "senha": inputSenha
        }

        const pattern = /^[\w.+\-]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
        fetchBody[pattern.test(inputUsuario) ? "email" : "nomeUsuario"] = inputUsuario;

        const fetchConfig = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(fetchBody)
        }

        fetch(`${basePath}/usuarios/login`, fetchConfig)
        .then((response) => {response.json()
            .then((data) => {
                const { token, secretKey } = data;

                if (token && secretKey) {
                    console.log(data)
                    setToken(data.token);
                    setSecretKey(data.secretKey);

                    navigate("/");
                }

                else {
                    alert(data.error);
                }
            })
        })
    }

    return (
        <>
        <Header />
        <main className={styles.loginMain}>
            <div className={styles.container}>
                <div className={styles.textDiv}>
                    <div className={styles.logoDiv}>
                        <img src="src/assets/Icone-Ingresso.svg" alt="" />
                        <h1>C<span>&#123;IN&#125;EMA</span></h1>
                    </div>
                    <h2>Escolha suas sessões, reserve seus lugares e entre de cabeça em narrativas que cativam e emocionam. Este é o nosso convite para você vivenciar o cinema de maneira única. Nossa página de login é a porta de entrada para essa experiência excepcional, e estamos empolgados para compartilhar cada momento cinematográfico com você.</h2>
                </div>
                <div className={styles.formDiv}>
                    <form onSubmit={handleLoginFromSubmit}>
                        <h1>Login</h1>
                        <h2>Faça login e garanta o seu lugar na diversão</h2>
                        <input id="usuario" type="text" placeholder="Usuário ou E-mail" required/>
                        <div className={styles.passwordDiv}>
                            <input type="password" placeholder="Senha" id="password" required />
                            <img onClick={showPassword} id="eye-icon" className={styles.eyeIcon} src="src/assets/eye.svg" alt="" />
                            <img onClick={showPassword} id="eye-slash-icon" className={styles.eyeIcon} style={{display: 'none'}} src="src/assets/eye-slash.svg" alt="" />
                        </div>
                        <a href="">Esqueci minha senha</a>

                        <button type="submit" className={styles.enterButton}>Entrar</button>

                        <hr />
                    </form>
                    <NavLink to="/registre-se" className={styles.registerButton}>Cadastre-se</NavLink>
                </div>
            </div>
        </main>
        <Footer />
        </>
    )
}