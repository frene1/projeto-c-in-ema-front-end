import styles from "./sessoes.module.css"
import { useSearchParams } from "react-router-dom";

import { Header } from "../../components/Header";
import { Footer } from "../../components/Footer";
import { BackgroundIngresso } from "../../components/BackgroundIngresso";
import { DropDownMenu } from "../../components/DropDownMenu";
import { FiltroSessoes } from "../../components/Sessoes/FiltroSessoes";
import { SessoesCard } from "../../components/Sessoes/sessoesCard";

import { ApiContext } from '../../contexts/ApiContextProvider';

import { useEffect, useState, useContext } from "react";

const cidades = [
    { key: "Rio de Janeiro", value: "rio de janeiro" },
    { key: "Niterói", value: "niteroi" },
    { key: "São Gonçalo", value: "sao goncalo" }
]

const bairros = {
    "rio de janeiro": [
        { key: "Ipanema", value: "ipanema" },
        { key: "Leblon", value: "leblon" },
        { key: "Copacabana", value: "copacabana" },
        { key: "Gávea", value: "gavea" },
        { key: "Botafogo", value: "botafogo" },
        { key: "Jardim Botânico", value: "jardim botânico" },
        { key: "Tijuca", value: "tijuca" },
        { key: "Lagoa", value: "lagoa" },
        { key: "Recreio dos Bandeirantes", value: "recreio dos bandeirantes" }
    ],
    "niteroi": [
        { key: "Icaraí", value: "icarai"},
        { key: "Santa Rosa", value: "santa rosa"},
        { key: "Fonseca", value: "fonseca"},
        { key: "Itaipu", value: "itaipu"},
        { key: "Centro", value: "centro"},
        { key: "São Francisco", value: "sao francisco"},
        { key: "Piratininga", value: "piratininga"},
        { key: "São Domingos", value: "sao domingos"}
    ],
    "sao goncalo": [
        { key: "Neves", value: "neves"},
        { key: "Alcântara", value: "alcantara"},
        { key: "Trindade", value: "trindade"},
        { key: "Estrela do Norte", value: "estrela do norte"},
        { key: "Paraíso", value: "paraiso"},
        { key: "Rio do Ouro", value: "rio do ouro"},
        { key: "Colubandê", value: "colubande"}
    ]
}

const sessoesTipo = [
    { key: "2d", value: 0 },
    { key: "3d", value: 1 },
    { key: "imax", value: 2 },
]

export function Sessoes() {
    const { basePath } = useContext(ApiContext);

    const [ searchParams, setSearchParams ] = useSearchParams();
    const filmeId = searchParams.get("id");

    const [ filme, setFilme ] = useState();

    const [ selectedCidade, setSelectedCidade ] = useState(null);
    const [ arrayBairros, setArrayBairros ] = useState([]);
    const [ selectedBairro, setSelectedBairro ] = useState(null);

    const [ selectedTipo, setSelectedTipo ] = useState(null);

    const [ listaSessoes, setListaSessoes ] = useState([]);
    const [ listaSessoesFiltradas, setListaSessoesFiltradas ] = useState([]);
    const [ sessoes, setSessoes ] = useState({});

    function loadFilme() {
        const url = `${basePath}/filmes/id`;
        const config = {
            method: "GET"
        }
        fetch(`${url}/${filmeId}`, config)
        .then(response => { response.json()
            .then(data => {
                setFilme(data);
            })
        })
    }

    function loadListaSessoes() {
        const url = "http://localhost:4000/sessoes";
        const config = {
            method: "GET"
        }
        fetch(`${url}/${filmeId}`, config)
        .then(response => { response.json()
            .then(data => {
                setListaSessoes(data);
                console.log("Data:", data);
            })
        })
    }

    function updateSessoes() {
        let updatedSessoesFiltradas = listaSessoes;

        console.log("Filtrando por:", selectedCidade, selectedBairro, selectedTipo);

        updatedSessoesFiltradas = updatedSessoesFiltradas.filter(
            (sessao) => sessao.cidade.toLowerCase() == selectedCidade
        )

        updatedSessoesFiltradas = updatedSessoesFiltradas.filter(
            (sessao) => sessao.bairro.toLowerCase() == selectedBairro
        )

        if (selectedTipo != null) {
            updatedSessoesFiltradas = updatedSessoesFiltradas.filter(
                (sessao) => sessao.tipo == selectedTipo
            )
        }

        setListaSessoesFiltradas(updatedSessoesFiltradas);

        let updatedSessoes = {}
        sessoesTipo.forEach((tipo) => {
            updatedSessoes[tipo.value] = updatedSessoesFiltradas.filter(
                (sessao) => sessao.tipo == tipo.value
            )
        })

        console.log(updatedSessoes);
        setSessoes(updatedSessoes);
    }

    function handleCidadeUpdate(newCidade) {
        setSelectedCidade(newCidade);
        
        const updatedBairros = bairros[newCidade];
        setArrayBairros(updatedBairros);
    }

    function handleBairrosUpdate(newBairro) {
        setSelectedBairro(newBairro);
    }

    function handleFiltroSessoesChange(value) {
        setSelectedTipo(value);
    }

    useEffect(() => {
        loadFilme()
        loadListaSessoes()
    }, []);
    useEffect(updateSessoes, [selectedCidade, selectedBairro, selectedTipo])

    console.log(listaSessoes);
    console.log(selectedTipo);


    return <>
        <Header/>

        <BackgroundIngresso/>

        <section className={styles.main}>
        <section className={styles.filme}>
            <div className={styles.filmeGradiente}/>
            <img className={styles.cartaz} src={filme && filme.urlImage}/>

            <div className={styles.filmeRightSide}>
                <section className={styles.filmeInfo}>
                    <div className={styles.filmeTitulo}>
                        <h1>{filme ? filme.titulo : "Titulo"}</h1>
                        <img src={filme && `src/assets/classificacao-indicativa/${filme.classificacao}.png`}/>
                    </div>
                    <h2>{filme ? filme.genero : "Genero"}</h2>
                    <p>{filme ? filme.descricao : "Descrição"}</p>
                </section>

                <section className={styles.dropDownMenusWrapper}>
                <section className={styles.dropDownMenus}>
                    <div className={styles.dropDownMenuWrapper}>
                    <DropDownMenu
                        className={styles.dropDownMenu}
                        title="Bairros"
                        items={arrayBairros}
                        disabled={selectedCidade === null}
                        onUpdate={handleBairrosUpdate}
                    />
                    </div>

                    <div className={styles.dropDownMenuWrapper}>
                    <DropDownMenu
                        title="Cidade"
                        items={cidades}
                        onUpdate={handleCidadeUpdate}
                    />
                    </div>
                </section>
                </section>
            </div>
        </section>

        <section className={styles.sessoes}>
            {listaSessoesFiltradas.length > 0 ?
                <>
                    <FiltroSessoes items={sessoesTipo} onChange={handleFiltroSessoesChange}/>

                    <section className={styles.sessoesCards}>
                        {
                            sessoesTipo.map((tipo, index) => {
                                return (sessoes[tipo.value].length > 0) &&
                                    <SessoesCard
                                        key={index}
                                        titulo={tipo.key}
                                        sessoes={sessoes[tipo.value]}
                                        filmeId={filme.id}
                                    />
                            })
                        }
                    </section>
                </>
                :
                (
                    (selectedCidade == null || selectedBairro == null) ?
                    <h1 className={styles.nenhumaSessaoEncontrada}>Selecione a cidade e o bairro.</h1>
                    :
                    <h1 className={styles.nenhumaSessaoEncontrada}>Nenhuma sessão encontrada.</h1>
                )
            }
        </section>
        </section>

        <Footer/>
    </>
}