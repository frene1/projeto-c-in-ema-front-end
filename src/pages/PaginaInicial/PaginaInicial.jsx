import { useState, useEffect, useContext } from 'react'
import styles from './paginainicial.module.css'
import { Header } from '../../components/Header'
import { Footer } from '../../components/Footer'
import { Slider } from '../../components/PaginaInicial/Slider'
import { SmallMovieCard } from '../../components/PaginaInicial/SmallMovieCard'
import { BackgroundIngresso } from "../../components/BackgroundIngresso"
import { NavLink } from 'react-router-dom'

import { ApiContext } from '../../contexts/ApiContextProvider';

export function PaginaInicial(){    
    const movies = [1,2,3,4,5]
    const [ listaFilmes, setListaFilmes ] = useState();

    const { basePath } = useContext(ApiContext);
    
    useEffect(() => {
        const url = `${basePath}/filmes` 
        const config = { 
            method: "GET", 
        }
        fetch(url, config) 
        .then(response => { response.json() 
            .then(data => {
                const updatedListaFilmes = data;
                console.log(data);
                setListaFilmes(updatedListaFilmes);
            })
        })
    }, []);

    return(
        <div className={styles.container}>
        <Header />

        <BackgroundIngresso/>

        <div className={styles.main}>
            <div className={styles.presentationCard}>
            <div className={styles.presentationCardText}>
                <h1>Transformando Filmes em Experiências Personalizadas</h1>
                <h2>Reserve Seu Assento e Viva a Magia do Cinema!</h2>
            </div>
            </div>
            
            <Slider />
            
            <h1 className={styles.moviesSectionTitle}>Em Cartaz</h1>
            
            <div className={styles.availableMoviesSection}>
                <div className={styles.availableMoviesRow}>
                    {listaFilmes &&
                        movies.map((movie, index) => {
                            var randInt = Math.floor(Math.random() * (listaFilmes.length - 1));
                            return(
                                <SmallMovieCard
                                    key={index}
                                    movieName={listaFilmes ? listaFilmes[randInt].titulo : ""}
                                    movieImage={listaFilmes ? listaFilmes[randInt].urlImage : "" }
                                    movieID={listaFilmes ? listaFilmes[randInt].id : ""}
                                />
                            )
                    })}
                </div>
                <NavLink to="/filmes" className={styles.availableMoviesSeeMore}>Ver Mais</NavLink>
            </div>
        </div>

        <Footer />
        </div>
    )
}


