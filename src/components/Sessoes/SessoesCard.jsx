import { useEffect, useState } from "react";
import styles from "./sessoesCard.module.css";
import { NavLink } from 'react-router-dom';

export function SessoesCard({ titulo, sessoes, filmeId }) {
    const [ sessoesArray, setSessoesArray ] = useState()

    useEffect(() => {
        setSessoesArray(sessoes.sort((a, b) => {
            a = a.horario.split(":").map((n) => parseInt(n));
            b = b.horario.split(":").map((n) => parseInt(n));

            if (a[0] > b[0]) {
                return 1;
            }
            else if (a[0] < b[0]) {
                return -1;
            }

            if (a[1] > b[1]) {
                return 1
            }
            else if (a[1] < [1]) {
                return -1;
            }

            return 0
        }))
    }, [sessoes]);

    return <div className={styles.card}>
        <div className={styles.titulo}>{titulo}</div>
        <section className={styles.sessoes}>
            {sessoesArray &&
                sessoesArray.map((sessao, index) => {
                    return <NavLink
                        key={index}
                        className={styles.sessao}
                        to={`/checkout?filmeId=${filmeId}&sessaoId=${sessao.id}`}
                        title="Ver Sessão"
                    >{sessao.horario}</NavLink>
                })
            }
        </section>
    </div>
}