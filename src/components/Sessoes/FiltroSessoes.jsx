import styles from "./filtroSessoes.module.css";
import { useState } from "react";

export function FiltroSessoes({ items, onChange }) {
    const [ selectedIndex, setSelectedIndex ] = useState(null);

    function handleOnClick(index) {
        const updatedSelectedIndex = selectedIndex === index ? null : index;
        setSelectedIndex(updatedSelectedIndex);

        onChange(updatedSelectedIndex === null ? null : items[updatedSelectedIndex].value);
    }

    return <div className={styles.filtros}>
        {items &&
            items.map((item, index) => {
                return <div
                    key={index}
                    className={`
                        ${styles.filtroButton}
                        ${index === selectedIndex ? styles.filtroButtonOn : ""}
                    `}
                    onClick={() => handleOnClick(index)}
                >{item.key}</div>
            })
        }
    </div>
}
