import styles from './header.module.css';
import { NavLink } from 'react-router-dom';
import { useContext } from "react";
import { ApiContext } from "../contexts/ApiContextProvider";

export function Header(){
    const { usuario } = useContext(ApiContext);

    return(
        <header className={styles.header}>
            <NavLink to="/">
                <div className={styles.logo}>
                    <img src="src/assets/Icone-Ingresso.svg" alt="" />
                    <h1>C<span>&#123;IN&#125;EMA</span></h1>
                </div>
            </NavLink>

            <div className={styles.nav}>
                <NavLink to ="/filmes"><img src="src/assets/Icone-Filmes.svg" /></NavLink>
                <NavLink to="/login"><img src="src/assets/Icone-Entrar.svg" /></NavLink>
                <NavLink to="/fale-conosco"><img src="src/assets/Icone-Ajuda.svg" /></NavLink>
            </div>

            {usuario &&
                <h1 className={styles.loggedMessage}>Bem Vindo, {usuario.nomeUsuario}</h1>
            }
        </header>
    )
}