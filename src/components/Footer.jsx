import styles from './footer.module.css'

export function Footer(){
    return(
        <footer className={styles.footer}>
            <div className={styles.footerColumn}>

                <div className={styles.footerInfo}>
                    <div className={styles.footerInfoRow}>
                        <h2>Endereço</h2>
                        <p>
                            <span>Av. Milton Tavares de Souza,</span>
                            <span>s/n - Sala 115 B - Boa Viagem,</span>
                            <span>Niterói - RJ </span>
                            <span>CEP: 24210-315</span>
                        </p>
                    </div>
                    <div className={styles.footerInfoRow}>
                        <h2>Fale Conosco</h2>
                        <p><span>contato@injunior.com.br</span></p>
                        <p className={styles.socialMediaIcons}>
                            <a href=""><img src="src/assets/Icone-Instagram.svg" alt="" /></a>
                            <a href=""><img src="src/assets/Icone-Facebook.svg" alt="" /></a>
                            <a href=""><img src="src/assets/Icone-Linkedin.svg" alt="" /></a>
                        </p>
                    </div>
                </div>
                
                <div className={styles.gridCentralizeWrapper}>
                    <img className={styles.footerLogo} src="src/assets/Icone-Footer.svg" alt="" />
                </div>

                <div className={styles.gridCentralizeWrapper}>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14700.760673396595!2d-43.1333917!3d-22.9063556!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ed79f10f3%3A0xb39c7c0639fbc9e8!2sIN%20Junior%20-%20Empresa%20Junior%20de%20Computa%C3%A7%C3%A3o%20da%20UFF!5e0!3m2!1spt-BR!2sbr!4v1692555236103!5m2!1spt-BR!2sbr" width="400" height="300" style={{border:0}} loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
                </div>
        
            </div>
            
            <div className={styles.footerCopyright}>
                <span>© Copyright 2023. IN Junior. Todos os direitos reservados. Niterói, Brasil.</span>
            </div>
        
        </footer>
    )
}