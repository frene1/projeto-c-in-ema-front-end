import styles from  "./smallmoviecard.module.css"
import { NavLink } from "react-router-dom";
export function SmallMovieCard(props){
    return(
        <div className={styles.smallMovieCard}>
            <div className={styles.smallMovieCardImage}>
                <img src={props.movieImage} alt="" />
            </div>
            <h2>{props.movieName}</h2>
            <div className={styles.smallMovieCardButton}>
                <NavLink to={"sessoes?id=" + props.movieID}>SESSÕES DISPONÍVEIS</NavLink>
            </div>
        </div>
    )
}