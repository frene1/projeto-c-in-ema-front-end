import styles from "./sliderItem.module.css";

export function SliderItem({ titulo, preco, descricao, urlImage }) {
    return <div className={styles.slide}>
        <img
            className={styles.slideBackground}
            src="src/assets/Card-Ingresso.svg"
        />
        
        <div className={styles.slideContent}>
            <img src={urlImage} />
            <div className={styles.slideText}>
                <div className={styles.slideTextTitulo}>
                    <h1>{titulo}</h1>
                    <h2>{preco}</h2>
                </div>
        
                <p>{descricao}</p>
            </div>
        </div>
    </div>
}