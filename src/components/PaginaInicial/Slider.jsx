import styles from "./slider.module.css"
import React, { useState } from 'react';

import { SliderItem } from "./SliderItem";

const items = [
    {
        titulo: "Pipoca (P)",
        preco: "R$ 11,99",
        descricao: 'Nada diz "cinema" como o aroma inconfundível de pipoca estourando. E quando se trata de uma porção de pipoca pequena, a magia se torna ainda mais irresistível. Imagine uma explosão de sabores em miniatura, onde cada grão é um pedacinho de felicidade.',
        urlImage: "src/assets/PaginaInicial/Pipoca.png"
    },
    {
        titulo: "Pipoca (G)",
        preco: "R$ 13,99",
        descricao: "No reino da experiência cinematográfica, uma rainha reina supremamente: a pipoca grande, uma verdadeira protagonista em cada tela de prata. Ela transcende o mero lanche e se transforma em uma jornada culinária que rivaliza com as tramas mais emocionantes.",
        urlImage: "src/assets/PaginaInicial/Pipoca.png"
    },
    {
        titulo: "Duo Pipoca (M) + Refri",
        preco: "R$ 11,99",
        descricao: 'Nada diz "cinema" como o aroma inconfundível de pipoca estourando. E quando se trata de uma porção de pipoca pequena, a magia se torna ainda mais irresistível. Imagine uma explosão de sabores em miniatura, onde cada grão é um pedacinho de felicidade.',
        urlImage: "src/assets/PaginaInicial/Promo.svg"
    }
]

export function Slider(){
    const [sliderIndex, setSliderIndex] = useState(0);
    function moveSliderLeft() {
        console.log("left");
        setSliderIndex(sliderIndex - 1);
    }
    
    function moveSliderRight() {
        console.log("right");
        setSliderIndex(sliderIndex + 1);
    }
    
    return(
        <div className={styles.container}>
            <div className={styles.sliderButtons}>
                <button
                    className={`${styles.arrow} ${styles.arrowLeft}`}
                    onClick={moveSliderLeft}
                    disabled={sliderIndex <= 0}
                >
                    <img src="src/assets/PaginaInicial/Arrow.svg"/>
                </button>
                <button
                    className={`${styles.arrow} ${styles.arrowRight}`}
                    onClick={moveSliderRight}
                    disabled={sliderIndex >= items.length - 1}
                >
                    <img src="src/assets/PaginaInicial/Arrow.svg"/>
                </button>
            </div>

            <div className={styles.sliderWrapper}>
            <div
                className={styles.slider}
                style={{ "--index": sliderIndex }}
            >
                {
                    items.map((item, index) => <SliderItem
                        key={index}
                        titulo={item.titulo}
                        preco={item.preco}
                        descricao={item.descricao}
                        urlImage={item.urlImage}
                    />)
                }
            </div>
            </div>
        </div>
    )
}