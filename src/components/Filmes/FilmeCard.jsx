import PropTypes from 'prop-types';

import styles from './filmeCard.module.css';

import { NavLink } from "react-router-dom";

export function FilmeCard({ filme }) {
    const descricaoCharLimit = 180;
    function sliceDescricao(descricao) {
        descricao = descricao.slice(0, descricaoCharLimit);
        
        for (let i = descricao.length - 1; i >= 0; i--) {
            if (descricao.charAt(i) == " ") {
                return descricao.slice(0, i);
            }
        }
    }

    return <div className={styles.filmeCard}>
        <section className={styles.sectionInfo}>
            <section className={styles.capa}>
                <img className={styles.cartazImg} src={filme.urlImage}/>
                
                <section className={styles.titulo}>
                    <h1>{filme.titulo}</h1>
                    <img src={`src/assets/classificacao-indicativa/${filme.classificacao}.png`}/>
                </section>
            </section>

            <section className={styles.descricao}>
                <p>{filme.genero}</p>
                <p>Direção: {filme.diretor}</p>
                <p>{filme.descricao.length < descricaoCharLimit ?
                filme.descricao
                :
                `${sliceDescricao(filme.descricao)} ...`
                }</p>
            </section>
        </section>

        <NavLink
            className={styles.verSessoesBotao}
            to={"/sessoes?id=" + filme.id}
            title="Ver Sessões"
        >VER SESSÕES</NavLink>
    </div>
}

FilmeCard.propTypes = {
    filme: PropTypes.object
}
