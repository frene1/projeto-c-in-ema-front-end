import { DropDownMenu } from '../../components/DropDownMenu';

import styles from "./search.module.css";

import { useState } from "react";

const generos = [
    { key: "Aventura", value: "aventura" },
    { key: "Ação", value: "ação" },
    { key: "Comédia", value: "comédia" },
    { key: "Crime", value: "crime" },
    { key: "Drama", value: "drama" },
    { key: "Fantasia", value: "fantasia" },
    { key: "Ficção", value: "ficção" },
    { key: "Ficção Científica", value: "ficção científica" },
];

const classificacoes = [
    { key: "Livre", value: 0},
    { key: "+10", value: 1},
    { key: "+12", value: 2},
    { key: "+14", value: 3},
    { key: "+16", value: 4},
    { key: "+18", value: 5},
]

export function Search ({ searchFilmes }) {
    const [ searchBar, setSearchBar ] = useState("");
    const [ selectedGenero, setSelectedGenero ] = useState(null);
    const [ selectedClassificacao, setSelectedClassificacao ] = useState(null);
    
    function handleSearchBarUpdate(event) {
        setSearchBar(event.target.value);
    }

    function handleClearButtonClick() {
        console.log("CLEAR!")
        setSearchBar("");
    }

    function handleSearchButtonClick() {
        searchFilmes({
            titulo: searchBar,
            genero: selectedGenero,
            classificacao: selectedClassificacao
        });
    }

    function handleSearchBarKeyUp({ key, target }) {
        if (key == 'Enter') {
            handleSearchButtonClick();
            target.blur();
        }
    }

    function handleGeneroUpdate(newGenero) {
        setSelectedGenero(newGenero);
        searchFilmes({
            titulo: searchBar,
            genero: newGenero,
            classificacao: selectedClassificacao
        });
    }

    function handleClassificacaoUpdate(newClassificacao) {
        setSelectedClassificacao(newClassificacao);
        searchFilmes({
            titulo: searchBar,
            genero: selectedGenero,
            classificacao: newClassificacao
        });
    }

    return <section className={styles.searchSection}>
    <section className={styles.searchSectionWrapper}>
        <section className={styles.searchBar}>
            <input type="text" placeholder="Pesquisar filmes" value={searchBar} onChange={handleSearchBarUpdate} onKeyUp={handleSearchBarKeyUp}/>
            
            <div className={styles.searchBarIconsWrapper}>
                {searchBar && 
                    <img
                        className={styles.clearButton}
                        onClick={handleClearButtonClick}
                        src="src/assets/Filmes/X.svg"
                        title="Limpar"
                    />
                }
                <div
                    className={styles.searchBarIcon}
                    onClick={handleSearchButtonClick}
                    title="Pesquisar"
                >
                    <img src="src/assets/Filmes/Lupa.svg"/>
                </div>
            </div>
        </section>

        <section className={styles.searchDropDownMenus}>
            <div className={styles.dropDownMenuWrapper}>
            <DropDownMenu
                title="Gênero"
                items={generos}
                onUpdate={handleGeneroUpdate}
            />
            </div>

            <div className={styles.dropDownMenuWrapper}>
            <DropDownMenu
                title="Classificação"
                items={classificacoes}
                onUpdate={handleClassificacaoUpdate}
            />
            </div>
        </section>
    </section>
    </section>
}