import styles from "./paginacaoButtons.module.css"

export function ArrowLeft({ onClick }) {
    return <img
        className={styles.arrowButton}
        onClick={onClick}
        src="src/assets/Filmes/LeftArrowButton.svg"
        title="Página Anterior"
    />
}

export function ArrowRight({ onClick }) {
    return <img
        className={styles.arrowButton}
        onClick={onClick}
        src="src/assets/Filmes/RightArrowButton.svg"
        title="Próxima Página"
    />
}

export function PageButton({ selected, index, onClick }) {
    return <div
        className={`${styles.pageButton} ${selected ? styles.pageButtonSelected : styles.pageButtonNotSelected}`}
        index={index}
        onClick={onClick}
    >{index}</div>
}

export function RetButton() {
    return <div className={`${styles.pageButton} ${styles.retButton}`}>...</div>
}
