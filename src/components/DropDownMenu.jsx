import PropTypes from 'prop-types';

import styles from './dropDownMenu.module.css';

import { useEffect, useState } from 'react';

export function DropDownMenu({ title, items=[], disabled=false, defaultValue=null, onUpdate }) {
    const [ selectedIndex, setSelectedIndex ] = useState(0);
    const [ opened, setOpened ] = useState(false);

    const defaultItem = { key: title, value: defaultValue };
    const arrayItems = [defaultItem].concat(items);

    function handleOpenButton(event) {
        setOpened(!disabled && !opened);
    }

    function handleMouseLeave(event) {
        setOpened(false);
    }

    function handleItemClick(event) {
        const index = parseInt(event.currentTarget.getAttribute('index'));
    
        setSelectedIndex(index);
        setOpened(false);
    }

    useEffect(() => {setSelectedIndex(0)}, [items]);
    useEffect(() => {onUpdate(arrayItems[selectedIndex].value)}, [selectedIndex]);

    return <>
        <div
            className={`
                ${styles.dropDownMenu}
                ${opened ? styles.dropDownMenuOpened : ""}
                ${disabled ? styles.dropDownMenuDisabled: ""}
            `}
            onMouseLeave={handleMouseLeave}
        >

            <div className={styles.buttonSelect} onClick={handleOpenButton}>
                <div className={styles.highLighBorder}/>
                <h1>{(selectedIndex >= 0 && selectedIndex < arrayItems.length) &&
                    arrayItems[selectedIndex].key
                }</h1>
                <img
                    src="src/assets/Filmes/Expand.svg"
                ></img>
            </div>

            {
                <div className={styles.items} style={{ "--items": arrayItems.length - 1 }}>
                <div className={styles.itemsWrapper}>
                    <div className={styles.highLighBorder}/>
                    
                    {arrayItems.map((item, index) =>
                        selectedIndex != index &&
                        <div
                            className={styles.item}
                            key={index}
                            index={index}
                            onClick={handleItemClick}
                        >{item.key}</div>
                    )}
                </div>
                </div>
            }
            
        </div>
    </>
}

DropDownMenu.propTypes = {
    title: PropTypes.string,
    items: PropTypes.array,
    disabled: PropTypes.bool,
    onUpdate: PropTypes.func
}
