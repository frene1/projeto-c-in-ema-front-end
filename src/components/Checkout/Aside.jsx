import styles from './aside.module.css'
import PropTypes from "prop-types"
import { Modal } from './Modal'
import { useState, useContext, useEffect } from 'react'

import { ApiContext } from '../../contexts/ApiContextProvider';

const sessoesTipo = ["2d", "3d", "imax"]

const fileiras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"]
const colunas = Array.from({length: 18}, (_, i) => i + 1)

export function Aside ({ filme, sessao, onSubmit, assentosSelecionados }) {

    const [openModal, setOpenModal] = useState(false)

    const { basePath } = useContext(ApiContext);

    function handleSubmit(event) {
        event.preventDefault();
        console.log("submit");

        const nome = document.getElementById("nome").value;
        const cpf = document.getElementById("cpf").value;
        console.log(nome, cpf);

        const url = `${basePath}/sessoes/assento`
        let fetchConfig = {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json"
            }
        }
        
        if (!assentosSelecionados && assentosSelecionados.length > 0) {
            return;
        }

        assentosSelecionados.forEach((assento) => {
            fetchConfig.body = JSON.stringify({
                "fileira": assento.fileira,
                "numero": assento.numero,
                "status": "comprado"
            })
            fetch(`${url}/${sessao.id}`, fetchConfig)
            .then((response) => {response.json()
                .then((data) => {
                    if (!data.error){
                        setOpenModal(true);
                        onSubmit();
                    }
                })
            })
        })
    }

    return (
        <aside className={styles.aside}>
            <div className={styles.contentCapa}>
                <div>
                    <img className={styles.moviePoster} src={filme.urlImage} />
                    <div>
                        <p>{filme.titulo}</p>
                        <div>
                            <span>{sessoesTipo[sessao.tipo]}</span>
                            <span>{sessao.horario}</span>
                        </div>
                    </div>
                </div>
            </div>
            
            <div className={styles.contentForm}>
                <div className={styles.title}>
                    <p> <img src="src/assets/chair.svg" /> Assentos escolhidos</p>
                </div>
                <div className={styles.frametitle}>
                    <form className={styles.commentForm} onSubmit={handleSubmit}>
                        <div className={styles.fromInputText}>
                            <label>NOME:</label>
                            <input id="nome" type="text" name="nome" placeholder="Digite seu nome" required/>
                            
                            <label>CPF:</label>
                            <input id="cpf" type="text" name="CPF" placeholder="Digite seu CPF" />
                        </div>
                        
                        <button type="submit">CONFIRMAR</button>
                    </form>
                </div>

                <Modal isOpen={openModal} setModalOpen={() => setOpenModal(!openModal)}>
                    <h1>Confirmação de Reserva!</h1>
                    <h2>Tem certeza que deseja confirma a reserva?</h2>
                </Modal>
            </div>
        </aside>
    )
}

Aside.PropTypes = {
    author: PropTypes.object
}