import styles from './room.module.css';
import { useState, useEffect, useContext } from "react";
import { ApiContext } from '../../contexts/ApiContextProvider';

const fileiras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
const assentosTipos = {
    "disponivel": "src/assets/Cadeira 1.1.svg",
    "selecionado": "src/assets/Cadeira 2.svg",
    "comprado": "src/assets/Cadeira 3.svg"
};

export function Assento({ assento, selecionado, handleOnClick }) {
    const [ selecionadoState, setSelecionadoState ] = useState(false);
    
    function handleButtonClick() {
        handleOnClick(assento)
    }

    useEffect(() => setSelecionadoState(selecionado), [selecionado])

    return <button className={styles.assentoButton} onClick={handleButtonClick} disabled={assento.status == "comprado"}>
        <img src={selecionadoState ? assentosTipos["selecionado"] : assentosTipos[assento.status]}/>
    </button>
}


export function Room ({ sessaoId, onChange, assentosSelecionados }) {
    const { basePath } = useContext(ApiContext);

    const [ listaAssentos, setListaAssentos ] = useState([]);

    function loadAssentos() {
        const url = `${basePath}/assentos`;
        const config = {
            method: "GET"
        }
        fetch(`${url}/${sessaoId}`, config)
        .then(response => { response.json()
            .then(data => {
                let updatedListaAssentos = [];
                fileiras.forEach((fileira) => {
                    updatedListaAssentos.push(data.filter((assento) => assento.fileira == fileira))
                })
                setListaAssentos(updatedListaAssentos);
            })
        })
    }

    useEffect(() => {
        loadAssentos();
    }, [assentosSelecionados]);

    return (
        <section className={styles.room}> 
            <div  className={styles.screen}>
                <p>Tela</p>
            </div>
            <div className={styles.ranks}>
                <ul>
                    {
                        fileiras.map((fileira, index) => <li key={index}>{fileira}</li>)
                    }
                </ul>

                <div className={styles.assentos}>
                    {
                        listaAssentos.map((fileira, index) => 
                            <div key={index} className={styles.fileiraAssentos}>
                                {listaAssentos &&
                                    fileira.map((assento, index) => {
                                        return <Assento
                                            key={index}
                                            assento={assento}
                                            selecionado={
                                                assentosSelecionados.find((item) => JSON.stringify(item) == JSON.stringify(assento)) !== undefined
                                            }
                                            handleOnClick={(assento) => onChange(assento)}
                                        />
                                    })
                                }
                            </div>
                        )
                    }
                </div>

                <ul>
                    {
                        fileiras.map((fileira, index) => <li key={index}>{fileira}</li>)
                    }
                </ul>
            </div>
            <hr />

            {/* -----------------------------subtitle/Legenda--------------------------------- */}

            <div className={styles.subtitle}>
                <p>
                    LEGENDA
                </p>
                <div>
                    <div>
                        <img src="src/assets/Cadeira 1.1.svg"/> 
                        <span>Disponível</span>
                    </div>
                    <div>
                        <img src="src/assets/Cadeira 2.svg"/> 
                        <span>Selecionado</span>
                    </div>
                    <div>
                        <img src="src/assets/Cadeira 3.svg"/> 
                        <span>Comprado</span>
                    </div>
                </div>
            </div>
        </section>
    )
}
