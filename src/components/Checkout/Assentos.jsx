import styles from "./assentos.module.css";
import { useState, useContext, useEffect } from "react"
import { ApiContext } from '../../contexts/ApiContextProvider';

const fileiras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];
const assentosTipos = {
    "disponivel": "src/assets/Cadeira 1.1.svg",
    "selecionado": "src/assets/Cadeira 2.svg",
    "comprado": "src/assets/Cadeira 3.svg"
}

export function Assento({ assento }) {
    return <img
        src={assentosTipos[assento.status]}
    />
}

export function Assentos({ sessaoId }) {    
    const { basePath } = useContext(ApiContext);

    const [ listaAssentos, setListaAssentos ] = useState();

    function loadAssentos() {
        const url = `${basePath}/assentos`;
        const config = {
            method: "GET"
        }
        fetch(`${url}/${sessaoId}`, config)
        .then(response => { response.json()
            .then(data => {
                let updatedListaAssentos = [];
                fileiras.forEach((fileira) => {

                    updatedListaAssentos.push(data.filter((assento) => assento.fileira == fileira))
                })
                setListaAssentos(updatedListaAssentos);
            })
        })
    }

    useEffect(loadAssentos, []);

    return <div className={styles.assentos}>
        {listaAssentos &&
            listaAssentos.map((fileira, index) => 
            <div key={index} >
                {
                    fileira.map((assento, index) =>
                        <Assento key={index} assento={assento}/>
                    )
                }
            </div>
            )
        }
    </div>
}