import { XCircle } from 'phosphor-react'
import styles from './modalConfirmada.module.css'
export function ModalConfirmada ({isOpen, setConfirmadaOpen, children}) {
    if(isOpen) {
        return (
            <div  className={styles.background}>
                <div  className={styles.modal}>
                    <div>
                        <div onClick={setConfirmadaOpen} className={styles.out}>
                            <XCircle size={32} />
                        </div>
                        {children}
                    </div>
                </div>
            </div>
        )
    }

    return null
}