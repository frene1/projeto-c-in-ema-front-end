import { ModalConfirmada } from './ModalConfirmada'
import styles from './modal.module.css'
import { useState } from 'react'

export function Modal ({isOpen, setModalOpen, children}) {

    const [openConfirmada, setOpenConfirmada] = useState(false)

    if(isOpen) {
        return (
            <div className={styles.background}>
                <div className={styles.modal}>
                    <div>
                        {children}
                        <div className={styles.modelButton}>
                            <button onClick={setModalOpen}>cancelar</button>
                            <button onClick={() => setOpenConfirmada(true)}>Confirmar</button>
                        </div>
                    </div>
                    <ModalConfirmada isOpen={openConfirmada} setConfirmadaOpen={() =>               setOpenConfirmada(!openConfirmada)} >
                        <h1>Reserva Confirmada!</h1>
                        <h2>Sua reserva foi confirmada com sucesso para a sessão selecionada.</h2>
                        <p>
                            Estamos felizes em telo conosco para esssa experiência cinematográfica. Prepare-se para se envolver em uma jornada emocionante na tela grande!
                        </p>
                    </ModalConfirmada>
                </div>
            </div>
        )
    }

    return null
}