import styles from "./backgroundIngresso.module.css";

export function BackgroundIngresso() {
    return <div className={styles.background}>
        <div className={styles.image}/>
    </div>
}
