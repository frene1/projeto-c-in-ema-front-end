import PropTypes from "prop-types";
import { createContext, useEffect, useState } from "react";

export const ApiContext = createContext();

export function ApiContextProvider({ children }) {
    const [ basePath, setBasePath ] = useState("http://localhost:4000");
    const [ token, setToken ] = useState();
    const [ secretKey, setSecretKey ] = useState();
    const [ usuario, setUsuario ] = useState();

    function resetLogin() {
        setToken();
        setSecretKey();
        setUsuario();
    }

    function getUser() {
        if (!token || !secretKey) {
            console.log("informação incompleta para o login")
            resetLogin();
            return;
        }

        console.log("procurando user");
        
        const fetchConfig = {
            method: "GET",
        }
        fetch(`${basePath}/usuarios?token=${token}&secretKey=${secretKey}`, fetchConfig)
        .then((response) => {response.json()
            .then((data) => {
                if (data.error) {
                    console.log("token expirado");
                    resetLogin();
                }
                else {
                    console.log("login efetuado");
                    setUsuario(data);
                }
            })
        })
    }

    useEffect(getUser, [token, secretKey]);

    return <ApiContext.Provider
        value={{
            basePath, setBasePath,
            token, setToken,
            secretKey, setSecretKey,
            usuario, setUsuario
    }}>
        {children}    
    </ApiContext.Provider>
}

ApiContextProvider.prpTypes = {
    children: PropTypes.node
}
